import tensorflow as tf
import random
import numpy as np
import cv2


def random_crop(img,croped_size):
    (th,tw) = croped_size
    h, w = img.shape[:2]
    x1 = random.randint(0, w - tw)
    y1 = random.randint(0, h - th)
    img_crop = img[y1:y1+th, x1:x1+tw]
    return img_crop


def crop_sub_imgs(x, is_random=True):
    n = len(x)
    x_c = []
    for i in range(n):
        x_n = random_crop(x[i],(96,96))
        x_n = x_n / (255. / 2.)
        x_n = x_n - 1.
        x_c.append(x_n)
    return x_c


def downsample(x):
    # We obtained the LR images by downsampling the HR images using bicubic kernel with downsampling factor r = 4.
    n = len(x)
    x_d = []
    for i in range(n):
        x_n = (x[i]+1.)*127.5
        x_n = cv2.resize(x_n, (24, 24),interpolation = cv2.INTER_CUBIC)
        x_n = x_n / (255. / 2.)
        x_n = x_n - 1.
        x_d.append(x_n)
    return x_d
