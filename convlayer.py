import tensorflow as tf

# from config import config, log_config
#
# img_path = config.TRAIN.img_path
import tensorflow.contrib.slim as slim
import scipy
import numpy as np
import cv2

def conv2d(batch_input, kernel=3, output_channel=64, stride=1, use_bias=True, act=None, name='conv'):
    # kernel: An integer specifying the width and height of the 2D convolution window
    with tf.variable_scope(name):
        if use_bias:
            return slim.conv2d(batch_input, output_channel, [kernel, kernel], stride, 'SAME', data_format='NHWC',
                            activation_fn=act, weights_initializer= tf.random_normal_initializer(stddev=0.02))
        else:
            return slim.conv2d(batch_input, output_channel, [kernel, kernel], stride, 'SAME', data_format='NHWC',
                            activation_fn=act, weights_initializer= tf.random_normal_initializer(stddev=0.02),
                            biases_initializer=None)

def batchnorm(inputs,name='bn',is_train=None):
    with tf.variable_scope(name):
        return slim.batch_norm(inputs, decay=0.9, epsilon=0.00001, updates_collections=tf.GraphKeys.UPDATE_OPS,
                        scale=False, fused=True, is_training=is_train)

def denselayer(inputs,output_size,act=None,name='dense'):
    with tf.variable_scope(name):
        output = tf.layers.dense(inputs, output_size, activation=act, kernel_initializer=tf.contrib.layers.xavier_initializer())
        return output

def lrelu(inputs, alpha,name='lrelu'):
    return tf.nn.leaky_relu(inputs,alpha=alpha,name=name)


def flattenlayer(inputs, name='flat'):
    with tf.variable_scope(name):
        return tf.contrib.layers.flatten(inputs,
            outputs_collections=None, scope=None)


def maxpool2d(inputs,kernel_size,name='pool'):
    return slim.max_pool2d(inputs, kernel_size, scope=name)


def pixelShuffler(inputs, scale=2,name='pshuffler'):
    with tf.variable_scope(name):
        size = tf.shape(inputs)
        batch_size = size[0]
        h = size[1]
        w = size[2]
        c = inputs.get_shape().as_list()[-1]

    # Get the target channel size
        channel_target = c // (scale * scale)
        channel_factor = c // channel_target

        shape_1 = [batch_size, h, w, channel_factor // scale, channel_factor // scale]
        shape_2 = [batch_size, h * scale, w * scale, 1]

    # Reshape and transpose for periodic shuffling for each channel
        input_split = tf.split(inputs, channel_target, axis=3)
        output = tf.concat([phaseShift(x, scale, shape_1, shape_2) for x in input_split], axis=3)

        return output

def phaseShift(inputs, scale, shape_1, shape_2):
    # Tackle the condition when the batch is None
    X = tf.reshape(inputs, shape_1)
    X = tf.transpose(X, [0, 1, 3, 2, 4])

    return tf.reshape(X, shape_2)

    