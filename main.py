#! /usr/bin/python
# -*- coding: utf8 -*-
import time
import os, time, pickle, random, time
from datetime import datetime
import numpy as np
from time import localtime, strftime
import logging

import tensorflow as tf

from model import generator, discriminator
from utils import *
import glob
import argparse

## config
batch_size = 8
lr_init = 1e-4
beta1 = 0.9
n_epoch = 20
# train_hr_images_path = './DIV2K_train_HR/DIV2K_train_HR/*.png'
train_hr_images_path = './training_images_numbered/*.jpg'
valid_hr_images_path = 'training_images/*.png'
valid_lr_images_path = 'lr_images/*.jpg'


def train(loss_mode):
    ## create folders to save trained model

    checkpoint_dir = "checkpoint" 
    if not os.path.exists(checkpoint_dir):
        os.mkdir(checkpoint_dir)

    ## load hr images
    train_hr_imgs = [cv2.imread(file) for file in sorted(glob.glob(train_hr_images_path))]

    ### DEFINE MODEL 

    ## train inference
    t_image = tf.placeholder('float32', [batch_size, 24, 24, 3], name='t_image_input_to_generator')
    t_target_image = tf.placeholder('float32', [batch_size, 96, 96, 3], name='t_target_image')

    net_g = generator(t_image, is_train=True, reuse=False)
    _, logits_real = discriminator(t_target_image, is_train=True, reuse=False)
    _, logits_fake = discriminator(net_g, is_train=True, reuse=True)

    ## trainning losses
    d_real_loss = tf.nn.sigmoid_cross_entropy_with_logits(
        labels=tf.ones_like(logits_real), logits=logits_real, name='d1')

    d_fake_loss = tf.nn.sigmoid_cross_entropy_with_logits(
        labels=tf.zeros_like(logits_fake), logits=logits_fake, name='d2')

    d_loss = tf.reduce_mean(d_real_loss + d_fake_loss)

    g_gan_loss = 1e-3 * tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(
        labels=tf.ones_like(logits_fake), logits=logits_fake, name='g'))

    mse_loss = tf.reduce_mean(tf.reduce_sum(tf.squared_difference(
        net_g, t_target_image), axis=[3]))

    g_loss = mse_loss +  g_gan_loss

    ## get trainable variables

    g_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, 'generator')
    d_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, 'discriminator') 


    with tf.variable_scope('learning_rate'):
        lr_v = tf.Variable(lr_init, trainable=False)

    ## MSE
    mse_step = tf.train.AdamOptimizer(lr_v, beta1=beta1).minimize(mse_loss, var_list=g_vars)
    ## SRGAN
    g_step = tf.train.AdamOptimizer(lr_v, beta1=beta1).minimize(g_loss, var_list=g_vars)
    d_step = tf.train.AdamOptimizer(lr_v, beta1=beta1).minimize(d_loss, var_list=d_vars)


    ## RESTORE MODEL
    sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True, log_device_placement=False))
    sess.run(tf.global_variables_initializer())
    
    if tf.train.get_checkpoint_state(checkpoint_dir+'/'):
        saver = tf.train.Saver()
        saver.restore(sess, checkpoint_dir+'/model')

    ## fixed learning rate
    sess.run(tf.assign(lr_v, lr_init))

    ## train GAN 
    for epoch in range(0, n_epoch):
        epoch_time = time.time()
        total_d_loss, total_g_loss, n_iter,errD,errG, errA = 0, 0, 0, 0, 0, 0

        for idx in range(0, len(train_hr_imgs), batch_size):
            step_time = time.time()
            b_imgs_96= crop_sub_imgs(train_hr_imgs[idx:idx + batch_size], is_random=True)
            b_imgs_24= downsample(b_imgs_96)
            if loss_mode == 'gan':
                ## update D
                errD, _ = sess.run([d_loss, d_step], {t_image: b_imgs_24, t_target_image: b_imgs_96})
                ## update G
                errG, errM, errA, _,_ = sess.run([g_loss, mse_loss, g_gan_loss, g_step, mse_step], {t_image: b_imgs_24, t_target_image: b_imgs_96})
                print("Epoch [%2d/%2d] %4d time: %4.4fs, d_loss: %.8f g_loss: %.8f (mse: %.6f adv: %.6f)" %
                  (epoch, n_epoch, n_iter, time.time() - step_time, errD, errG, errM, errA))
               

            if loss_mode == 'mse':               
                ## update G only
                errM,_ = sess.run([mse_loss,mse_step], {t_image: b_imgs_24, t_target_image: b_imgs_96}) 
                print("Epoch [%2d/%2d] %4d time: %4.4fs, mse: %.6f" %
                  (epoch, n_epoch, n_iter, time.time() - step_time, errM))
            
            n_iter += 1
        ## save model

        if (epoch != 0) and (epoch % 10 == 0):
            saver = tf.train.Saver()
            saver.save(sess, checkpoint_dir+'/model', write_meta_graph=False)

      


def test():
    ## create folders to save test images
    save_dir = "test"
    if not os.path.exists(save_dir):
        os.mkdir(save_dir)
    checkpoint_dir = "checkpoint"

    valid_lr_img = cv2.imread('C:/Users/Jason/Repos/442project/lr_images/1.jpg',0)

    valid_lr_img1 = (valid_lr_img / 127.5) - 1  # rescale to ï¼»ï¼1, 1]
    # print(valid_lr_img.min(), valid_lr_img.max())

    size = valid_lr_img1.shape
    print(size)

    t_image = tf.placeholder('float32', [1,None,None,3], name='input_image')


    net_g = generator(t_image, is_train=False, reuse=False)

    ###========================== RESTORE G =============================###

    sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True, log_device_placement=False))
    sess.run(tf.global_variables_initializer())
    saver=tf.train.Saver()
    saver.restore(sess, checkpoint_dir+'/model')

    ###======================= EVALUATION =============================###
    start_time = time.time()
    out = sess.run(net_g, {t_image: [valid_lr_img1]})
    out_img = (out[0] + 1 ) * 127.5 
    cv2.imwrite(save_dir + '/valid_gen.png',out_img)
    cv2.imwrite(save_dir + '/valid_lr.png',valid_lr_img)
  #  cv2.imwrite(save_dir + '/valid_hr.png',valid_hr_img)

    # bicubic interpolation
    out_bicu = cv2.resize(valid_lr_img, (size[1] * 4, size[0] * 4), interpolation = cv2.INTER_CUBIC)
    cv2.imwrite( save_dir + '/valid_bicubic.png',out_bicu)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--mode', type=str, default='train', help='train, test')
    parser.add_argument('--loss', type=str, default='mse', help='mse, gan')

    args = parser.parse_args()

    if args.mode == 'train':
        train(args.loss)
    elif args.mode == 'test':
        test()
    else:
        raise Exception("Unknown --mode")

