# # import the necessary packages
# import cv2
# import imutils

# class ShapeDetector:
# 	def __init__(self):
# 		pass

# 	def detect(self, c):
# 		# initialize the shape name and approximate the contour
# 		shape = "unidentified"
# 		peri = cv2.arcLength(c, True)
# 		approx = cv2.approxPolyDP(c, 0.04 * peri, True)

# 		# if the shape is a triangle, it will have 3 vertices
# 		if len(approx) == 3:
# 			shape = "triangle"

# 		# if the shape has 4 vertices, it is either a square or
# 		# a rectangle
# 		elif len(approx) == 4:
# 			# compute the bounding box of the contour and use the
# 			# bounding box to compute the aspect ratio
# 			(x, y, w, h) = cv2.boundingRect(approx)
# 			ar = w / float(h)

# 			# a square will have an aspect ratio that is approximately
# 			# equal to one, otherwise, the shape is a rectangle
# 			shape = "square" if ar >= 0.95 and ar <= 1.05 else "rectangle"

# 		# if the shape is a pentagon, it will have 5 vertices
# 		elif len(approx) == 5:
# 			shape = "pentagon"

# 		# otherwise, we assume the shape is a circle
# 		else:
# 			shape = "circle"

# 		# return the name of the shape
# 		return shape



# def get_corners(image):
# 	'''Get the 4 corners of the document.'''
# 	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# 	blurred = cv2.GaussianBlur(gray, (5, 5), 0)
# 	thresh = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]
# 	cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
# 		cv2.CHAIN_APPROX_SIMPLE)
# 	cnts = imutils.grab_contours(cnts)
# 	sd = ShapeDetector()

# 	for c in cnts:
# 		# compute the center of the contour, then detect the name of the
# 		# shape using only the contour
# 		M = cv2.moments(c)
# 		cX = int((M["m10"] / M["m00"]) * ratio)
# 		cY = int((M["m01"] / M["m00"]) * ratio)
# 		shape = sd.detect(c)

# 		# multiply the contour (x, y)-coordinates by the resize ratio,
# 		# then draw the contours and the name of the shape on the image
# 		c = c.astype("float")
# 		c *= ratio
# 		c = c.astype("int")
# 		cv2.drawContours(image, [c], -1, (0, 255, 0), 2)
# 		cv2.putText(image, shape, (cX, cY), cv2.FONT_HERSHEY_SIMPLEX,
# 			0.5, (255, 255, 255), 2)

# 		# show the output image
# 		cv2.imshow("Image", image)
# 		cv2.waitKey(0)

import numpy as np
import cv2
import matplotlib.pyplot as plt

def get_corners(img1):
	pts = []
	fig = plt.figure()
	ax = fig.add_subplot(111)
	coords = []
	def onclick(event):
		global ix, iy
		ix, iy = event.xdata, event.ydata
		coords.append((ix, iy))

		if len(coords) == 4:
			fig.canvas.mpl_disconnect(cid)
			plt.close()
		return coords

	ax.imshow(img1)
	cid = fig.canvas.mpl_connect('button_press_event', onclick)
	plt.show()
	pts = coords
	return pts
