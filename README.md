# Implementation of SRGAN #



### Download the dataset ###

- Training set (hr image): [DIV2K_train_HR](http://data.vision.ee.ethz.ch/cvl/DIV2K/DIV2K_train_HR.zip)

- Test set (hr image): [DIV2K_valid_HR](http://data.vision.ee.ethz.ch/cvl/DIV2K/DIV2K_valid_HR.zip)

- Test set (lr image): [DIV2K_valid_LR_bicubic_X4](http://data.vision.ee.ethz.ch/cvl/DIV2K/DIV2K_valid_LR_bicubic_X4.zip)

### What is this repository for ###


Implementation of ["Photo-Realistic Single Image Super-Resolution Using a Generative Adversarial Network"](https://arxiv.org/pdf/1609.04802.pdf)


### Denpendency ###

- python 3.6
- tensorflow 1.12.0 (tensorflow gpu version is recommended)
- opencv 3.4.3


### Run ###
- Set Image Path in ``` main.py ```

```bash
train_hr_images_path = 'your_image_path/DIV2K_train_HR/*.png'
valid_hr_images_path = 'your_image_path/DIV2K_valid_HR/*.png'
valid_lr_images_path = 'your_image_path/DIV2K_valid_LR_bicubic_X4/X4/*.png'
```


- Train

```bash
python main.py 
```
or

```python main.py --mode='train'
 ```


- Test

```bash
python main.py --mode='test'
```

