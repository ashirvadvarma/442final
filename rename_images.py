import cv2
import glob

images_path = "./training_images/*.jpg"

i = 1
for file in glob.glob(images_path):
    image = cv2.imread(file)

    if (i < 10):
        name = "00" + str(i)
    elif (i < 100):
        name = "0" + str(i)
    else:
        name = str(i)

    cv2.imwrite(name + ".jpg", image)
    i += 1