import cv2
import os
import multiprocessing
from functools import partial
import eta.core.image as etai


INPUT_FOLDER = '../training_images'
OUTPUT_FOLDER = '../training_images_resized'


def get_image_folder():
	'''Get the path to the folder holding all the images.'''

	current_path = os.getcwd()
	image_folder = INPUT_FOLDER
	return os.path.join(current_path, image_folder)


def get_list_of_images(image_folder):
	'''Given path to images, return list of image files.'''

	return os.listdir(image_folder)


def get_output_folder():
	'''Get the path to the output folder.'''

	current_path = os.getcwd()
	output_folder = OUTPUT_FOLDER
	result = os.path.join(current_path, output_folder)

	'''Check if output directory already exists. If not, create it.'''
	if not os.path.exists(result):
		os.makedirs(result)


	return os.path.join(current_path, output_folder)


def get_output_filename(filename, image_type):
	'''Get output image filename.'''

	name = filename.split('.')[0]
	return name + '.' + image_type


def process(filename, dimensions, image_type):
	'''Resize and reformat the image provided.'''

	'''Get the path to the image.'''
	file_path = os.path.join(get_image_folder(), filename)

	'''Read the image.'''
	img = etai.read(file_path)

	'''Resize the image.'''
	resized = etai.resize(img, width=dimensions[0], height=dimensions[1])

	'''Get the output image filename.'''
	out_filename = get_output_filename(filename, image_type)

	'''Get the path to where the image should be saved to.'''
	out_path = os.path.join(get_output_folder(), out_filename)

	'''Save the image.'''
	etai.write(resized, out_path)


def get_dimensions():
	'''Get the dimensions from user.'''

	user_input = input('What should be the dimensions? (width height) ')
	dim = user_input.split(' ')
	result = (int(dim[0]), int(dim[1]))
	return result


def get_format():
	'''Get image format.'''
	
	user_input = input('What should be the image format type? (png, jpg, etc.) ')
	return user_input


def main():
	'''Run main.'''

	'''Get the folder with the input images.'''
	image_folder = get_image_folder()

	'''Get the filenames for the images in the input folder.'''
	images = get_list_of_images(image_folder)

	'''Get the dimensions of the images to be resized to.'''
	dimensions = get_dimensions()

	'''Get the format that the images should be saved as.'''
	form = get_format()

	'''Multiprocess so that we can process multiple images at once to speed it up.'''
	with multiprocessing.Pool() as pool:
		pool.map(partial(process, dimensions=dimensions, image_type=form), images)


main()