import eta.core.image as etai
from get_corners import get_corners
from align_image import align_document
import cv2
import os
import img2pdf
import tensorflow as tf
from model import generator


def get_filename():
	'''Get filename from user.'''

	filename = input('What file do you want to use? ')
	return filename


def get_output_type():
	'''Get output format from user.'''

	result = input('What format should this be? (pdf, png, etc.) ')
	return result


def apply_srgan(img):
	'''Apply SRGAN method onto img.'''

	checkpoint_dir = os.path.join(os.getcwd(), 'checkpoint')
	model_dir = os.path.join(checkpoint_dir, 'model')

	valid_lr = (img / 127.5) - 1
	size = valid_lr.shape
	t_image = tf.placeholder('float32', [1, None, None, 3], name='input_image')

	net_g = generator(t_image, is_train=False, reuse=False)

	sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True, log_device_placement=False))
	sess.run(tf.global_variables_initializer())
	saver=tf.train.Saver()
	saver.restore(sess, model_dir)
	out = sess.run(net_g, {t_image: [valid_lr]})
	out_img = (out[0] + 1 ) * 127.5 
	return out_img


def convert_to_gray(img):
	'''Convert input image into grayscale.'''

	return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


def main():
	'''Run main.'''
	filename = get_filename()
	# out_type = get_output_type()
	#image = etai.read(filename, flag=cv2.IMREAD_GRAYSCALE)
	image = cv2.imread(filename, cv2.IMREAD_COLOR)
	corners = get_corners(image)
	aligned_image = align_document(image, corners)
	aligned_image = apply_srgan(aligned_image)
	aligned_image = convert_to_gray(aligned_image)
	print('Your image is saved as out_image.png and out_doc.pdf')

	path = os.path.join(os.getcwd(), 'out_image.png')
	etai.write(aligned_image, path)

	with open('out_doc.pdf',"wb") as f:
		f.write(img2pdf.convert('out_image.png'))


main()