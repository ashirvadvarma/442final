
import time
import tensorflow as tf
from convlayer import *



def generator(t_image, is_train=False, reuse=False):

    with tf.variable_scope("generator", reuse=reuse):

        net = conv2d(t_image, 9, 64, 1 , act = None,name='conv1')
        net = tf.nn.relu(net)
        temp = net

        # B residual blocks
        for i in range(16):
            res = conv2d(net, 3, 64, 1,  use_bias=False,act=None, name='resconv1%s' % i)
            res = batchnorm(res, name='resb1/%s' % i,is_train=is_train)
            res = tf.nn.relu(res)
            res = conv2d(res, 3, 64, 1, use_bias=False, act=None, name='resconv2/%s' % i)
            res = batchnorm(res,  name='resb2/%s' % i,is_train=is_train)
            res = tf.add(res,net)
            net = res

        net = conv2d(net, 3, 64, 1, act=None, name='n64s1/c/m')
        net = batchnorm(net,  name='n64s1/b/m',is_train=is_train)
        net = tf.add(net, temp)
        # B residual blacks end
        with tf.variable_scope('subpixelconv_stage1'):
            net = conv2d(net, 3, 256, 1, act=None, name='n256s1/1')
            net = pixelShuffler(net, scale=2)
            net = tf.nn.relu(net)
        with tf.variable_scope('subpixelconv_stage2'):
            net = conv2d(net, 3, 256, 1, act=None, name='n256s1/2')
            net = pixelShuffler(net, scale=2)
            net = tf.nn.relu(net)
            
        net = conv2d(net, 1, 3, 1, act=tf.nn.tanh, name='out')
        return net


def discriminator(input_images, is_train=True, reuse=False):


    with tf.variable_scope("discriminator", reuse=reuse):
  
        net = conv2d(input_images,1,64, 1, act=None, name='d_conv1')
        net = lrelu(net,0.2,name='lrelu_1')
        net = conv2d(net, 3, 64 , 2, act=None,name='d_conv2')
        net = batchnorm(net, name='h1/bn',is_train=is_train)
        net = lrelu(net,0.2,name='lrelu_2')
        net = conv2d(net, 3, 128, 1, act=None, name='d_conv3')
        net = batchnorm(net, name='h2/bn',is_train=is_train)
        net = lrelu(net, 0.2,name='lrelu_3')
        net = conv2d(net, 3, 128, 2, act=None, name='d_conv4')
        net = batchnorm(net, name='h3/bn',is_train=is_train)
        net = lrelu(net, 0.2,name='lrelu_4')
        net = conv2d(net, 3, 256, 1, act=None, name='d_conv5')
        net = batchnorm(net, name='h4/bn',is_train=is_train)
        net = lrelu(net, 0.2,name='lrelu_5')
        net = conv2d(net, 3, 256, 2, act=None, name='d_conv6')
        net = batchnorm(net, name='h5/bn',is_train=is_train)
        net = lrelu(net, 0.2,name='lrelu_6')
        net = conv2d(net, 3, 512, 1, act=None, name='d_conv7')
        net = batchnorm(net, name='h6/bn',is_train=is_train)
        net = lrelu(net, 0.2,name='lrelu_7')
        net = conv2d(net, 3, 512, 2, act=None, name='d_conv8')
        net = batchnorm(net,  name='h7/bn',is_train=is_train)
        net = lrelu(net, 0.2,name='lrelu_8')

        net = flattenlayer(net, name='flatten')
        net = denselayer(net, 1024, act=None,name='dense_layer_1')
        net = lrelu(net,0.2,name='lrelu_9')
        net = denselayer(net, 1, act=None,name='dense_layer_2')
        logits = net
        net = tf.nn.sigmoid(net)


    return net, logits

'''
def Vgg19_simple_api(rgb, reuse):
    """
    Build the VGG 19 Model

    Parameters
 
    VGG_MEAN = [103.939, 116.779, 123.68]
    with tf.variable_scope("VGG19", reuse=reuse) as vs:
        start_time = time.time()
        print("build model started")
        rgb_scaled = rgb * 255.0
        # Convert RGB to BGR
        if tf.__version__ <= '0.11':
            red, green, blue = tf.split(3, 3, rgb_scaled)
        else:  # TF 1.0
            # print(rgb_scaled)
            red, green, blue = tf.split(rgb_scaled, 3, 3)
        assert red.get_shape().as_list()[1:] == [224, 224, 1]
        assert green.get_shape().as_list()[1:] == [224, 224, 1]
        assert blue.get_shape().as_list()[1:] == [224, 224, 1]
        if tf.__version__ <= '0.11':
            bgr = tf.concat(3, [
                blue - VGG_MEAN[0],
                green - VGG_MEAN[1],
                red - VGG_MEAN[2],
            ])
        else:
            bgr = tf.concat(
                [
                    blue - VGG_MEAN[0],
                    green - VGG_MEAN[1],
                    red - VGG_MEAN[2],
                ], axis=3)
        assert bgr.get_shape().as_list()[1:] == [224, 224, 3]
        """ input layer """

        """ conv1 """
        network = conv2d(bgr, 3, 64, 1, act=tf.nn.relu, name='conv1_1')
        network = conv2d(network, 3, 64,1, act=tf.nn.relu, name='conv1_2')
        network = maxpool2d(network, [2, 2], name='pool1')
        """ conv2 """
        network = conv2d(network, 3, 128, 1, act=tf.nn.relu, name='conv2_1')
        network = conv2d(network, 3, 128, 1,  act=tf.nn.relu, name='conv2_2')
        network = maxpool2d(network, [2, 2], name='pool2')
        """ conv3 """
        network = conv2d(network, 3, 256, 1, act=tf.nn.relu, name='conv3_1')
        network = conv2d(network, 3, 256, 1, act=tf.nn.relu, name='conv3_2')
        network = conv2d(network, 3, 256, 1, act=tf.nn.relu, name='conv3_3')
        network = conv2d(network, 3, 256, 1, name='conv3_4')
        network = maxpool2d(network, [2, 2], name='pool3')
        """ conv4 """
        network = conv2d(network, 3, 512, 1, act=tf.nn.relu, name='conv4_1')
        network = conv2d(network, 3, 512, 1, act=tf.nn.relu, name='conv4_2')
        network = conv2d(network, 3, 512, 1, act=tf.nn.relu, name='conv4_3')
        network = conv2d(network, 3, 512, 1, act=tf.nn.relu, name='conv4_4')
        network = maxpool2d(network, [2, 2], name='pool4')  # (batch_size, 14, 14, 512)
        conv = network
        """ conv5 """
        network = conv2d(network, 3, 512, 1, act=tf.nn.relu, name='conv5_1')
        network = conv2d(network, 3, 512, 1, act=tf.nn.relu, name='conv5_2')
        network = conv2d(network, 3, 512, 1,act=tf.nn.relu,  name='conv5_3')
        network = conv2d(network, 3, 512, 1, act=tf.nn.relu,  name='conv5_4')
        network = maxpool2d(network, [2, 2], name='pool5')  # (batch_size, 7, 7, 512)
        """ fc 6~8 """
        network = flattenlayer(network, name='flattenvgg')
        network = denselayer(network, 4096, act=tf.nn.relu, name='fc6')
        network = denselayer(network, 4096, act=tf.nn.relu, name='fc7')
        network = denselayer(network, 1000, act=tf.identity, name='fc8')
        print("build model finished: %fs" % (time.time() - start_time))
        return network, conv
'''
